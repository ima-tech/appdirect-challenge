# Release notes

## Functionalities

- Lists the tweets of three accounts (appdirect, laughingsquid, techcrunch)
- Each tweet can be clicked on to be open on Twitter
- The mentioned username can be clicked on to be open on Twitter
- If a tweet is quoted, the quoted tweet is shown in a Twitter-style format
- The number of retweets and favorites are shown for each tweet
- The created date is shown and formatted for each tweet
- A 3-column interface for desktops and tablets, and 1 column for web
- Responsive design
- Supports three themes: 'Light', 'Dark', 'Blue'
- Saving and retrieving of the parameters using the LocalStorage
- Drag and Drop to reorder the columns
- Search filtering of the tweets in each column.
- Define a common time range for all the columns
- Show the number of tweets in each column
- Simple Gulp command to build the project
- Auto-generated documentation
- Execution of tests using Jasmine and Karma


## External Frameworks used

This here lists all the external frameworks used for this application

### Core
- AngularJS 1.6
- Bootstrap 3.3
- FontAwesome 4.7
- Angular Bootstrap DateTimePicker
- Angular Sortable View
- Angular Ui Bootstrap
- Angular Ui Router

### Building / Tests
- Gulp 3.9
- Jasmine 2.6
- Karma 1.7
- JSDoc 3.4
- PhantomJS 2.1