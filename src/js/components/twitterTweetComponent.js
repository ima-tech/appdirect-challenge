/**
 * @class twitterTweet
* @memberOf appdirect.challenge
*/
angular.module('appdirect.challenge').component('twitterTweet', {
    templateUrl: 'partials/twitterTweet.html',
    bindings: {
        tweet : '='
    },
    controller: function() {
        var self = this;

        /**
         * @name convertToDate
         * @function
         * @memberOf appdirect.challenge.twitterTweet
         *
         * @description
         * This is the function used to return a Date object from a String representation of a date.
         */
        self.convertToDate = function(date) {
            return new Date(date);
        };

        /**
         * @name getTweetUrl
         * @function
         * @memberOf appdirect.challenge.twitterTweet
         *
         * @description
         * This is the function used to return the url used to link to the tweet.
         */
        self.getTweetUrl = function() {
            return "https://twitter.com/statuses/" + self.tweet.id_str;
        };

        /**
         * @name isTweetQuoted
         * @function
         * @memberOf appdirect.challenge.twitterTweet
         *
         * @description
         * This is the function used to return a boolean if the tweet is quoted or not.
         */
        self.isTweetQuoted = function() {
            return self.tweet.is_quote_status;
        }
    }
});