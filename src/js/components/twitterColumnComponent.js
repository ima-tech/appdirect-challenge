/**
 * @class twitterColumn
 * @memberOf appdirect.challenge
 */

angular.module('appdirect.challenge').component('twitterColumn', {
    templateUrl: 'partials/twitterColumn.html',
    bindings: {
        tweets : '=',
        tweetColumn : '='
    },
    controller : function() {
        var self = this;
        
        /**
         * @name getColumnImage
         * @function
         * @memberOf appdirect.challenge.twitterColumn
         *
         * @description
         * This is the function used to return the proper banner image url of the twitter account.
         */
        self.getColumnImage = function() {
            if(self.tweets != undefined && self.tweets.length > 0) {
                return self.tweets[0].user.profile_banner_url;
            }
        }
    }
});