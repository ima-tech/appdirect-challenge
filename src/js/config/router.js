/**
 * @class config
 * @memberOf appdirect.challenge
 */
angular.module('appdirect.challenge').config( [ "$stateProvider", "$urlRouterProvider", function( $stateProvider, $urlRouterProvider ) {

    $urlRouterProvider.otherwise( "/" ); //Redirect to home if the url doesn't match

    // Define here all the routes
    $stateProvider
        .state("Home",
            {
                url: "/",
                templateUrl: "partials/home.html"
            }
        )
        .state("Edit",
            {
                url: "/edit",
                templateUrl: "partials/edit.html"
            }
        )
    ;
} ]);