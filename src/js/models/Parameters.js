/**
 * Represents the parameters of the application.
 * @class Parameters
 *
 * @memberOf appdirect.challenge.models
 *
 *
 *  @property {Number}          numberOfTweets              - The number of tweets to show on the page.
 *  @property {Date}            startDate                   - The date for which all the tweet must be greater than to be shown on the page.
 *  @property {Boolean}         isStartDate                 - The boolean determining if there is a start date or not.
 *  @property {Date}            endDate                     - The date for which all the tweet must be lesser than to be shown on the page.
 *  @property {Boolean}         isEndDate                   - The boolean determining if there is an end date or not.
 *  @property {String}          style                       - The style choosen by the user. It can be any of these three values: 'Light', 'Dark', 'Blue'
 *
 */
function Parameters() {
    var self = this;

    self.numberOfTweets = 30;
    self.startDate;
    self.isStartDate = false;
    self.endDate;
    self.isEndDate = false;
    self.style = "Dark";

    /**
     * @name init
     * @function
     * @memberOf appdirect.challenge.models.Parameters
     *
     * @description
     * This is the function used to init the object.
     */
    self.init = function() {
    };

    /**
     * @name setNumberOfTweets
     * @function
     * @memberOf appdirect.challenge.models.Parameters
     *
     * @description
     * This is a setter for the numberOfTweets property.
     */
    self.setNumberOfTweets = function(numberOfTweets) {
        if(!angular.isNumber(numberOfTweets)) {
            throw("The numberOfTweets you passed is not a number. You passed: '" + typeof(numberOfTweets) + "'.")
        }

        self.numberOfTweets = numberOfTweets;
    };

    /**
     * @name setIsStartDate
     * @function
     * @memberOf appdirect.challenge.models.Parameters
     *
     * @description
     * This is a setter for the isStartDate property.
     */
    self.setIsStartDate = function(isStartDate) {
        if(isStartDate !== true && isStartDate !== false) {
            throw("The isStartDate parameter you passed is not a boolean. You passed: '" + typeof(isStartDate) + "'.")
        }

        self.isStartDate = isStartDate;
    };

    /**
     * @name setIsEndDate
     * @function
     * @memberOf appdirect.challenge.models.Parameters
     *
     * @description
     * This is a setter for the isEndDate property.
     */
    self.setIsEndDate = function(isEndDate) {
        if(isEndDate !== true && isEndDate !== false) {
            throw("The isEndDate parameter you passed is not a boolean. You passed: '" + typeof(isEndDate) + "'.")
        }

        self.isEndDate = isEndDate;
    };

    /**
     * @name setEndDate
     * @function
     * @memberOf appdirect.challenge.models.Parameters
     *
     * @description
     * This is a setter for the endDate property.
     */
    self.setEndDate = function(endDate) {
        if(!angular.isDate(endDate) && endDate != undefined) {
            throw("The endDate you passed is not a date. You passed: '" + typeof(endDate) + "'.")
        }

        self.endDate = endDate;
    };

    /**
     * @name setStartDate
     * @function
     * @memberOf appdirect.challenge.models.Parameters
     *
     * @description
     * This is a setter for the startDate property.
     */
    self.setStartDate = function(startDate) {
        if(!angular.isDate(startDate) && startDate != undefined) {
            throw("The startDate you passed is not a date. You passed: '" + typeof(startDate) + "'.")
        }

        self.startDate = startDate;
    };

    /**
     * @name setStyle
     * @function
     * @memberOf appdirect.challenge.models.Parameters
     *
     * @description
     * This is a setter for the setStyle property.
     */
    self.setStyle = function(style) {
        if(!angular.isString(style)) {
            throw("The style you passed is not a string. You passed: '" + typeof(style) + "'.")
        }

        if(style != "Dark" && style != "Light" && style != "Blue") {
            throw("The style value you passed is invalid. It should be either 'Dark', 'Light' or 'Blue'. You passed: '" + style + "'.")
        }

        self.style = style;
    };

    /**
     * @name deserialize
     * @function
     * @memberOf appdirect.challenge.models.Parameters
     *
     * @description
     * This is the method used to deserialize a JSON string into the current object.
     */
    self.deserialize = function(serializedString) {
        var parameter = JSON.parse(serializedString);

        // Reconvert it back to our object
        self.setNumberOfTweets(parameter.numberOfTweets);
        self.setIsStartDate(parameter.isStartDate);
        self.setIsEndDate(parameter.isEndDate);
        self.setStyle(parameter.style);

        if(parameter.startDate != undefined) {
            self.setStartDate(new Date(parameter.startDate));
        }

        if(parameter.endDate != undefined) {
            self.setEndDate(new Date(parameter.endDate));
        }
    };

    /**
     * @name serialize
     * @function
     * @memberOf appdirect.challenge.models.Parameters
     *
     * @description
     * This is the method used to serialize the current object in JSON.
     */
    self.serialize = function() {
        return JSON.stringify(this);
    };

    self.init();
};