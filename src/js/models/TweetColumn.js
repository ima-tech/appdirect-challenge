/**
 * Represents a TweetColumn in the application
 * @class TweetColumn
 *
 * @memberOf appdirect.challenge.models
 *
 *
 *  @property {Number}          screenName              - The number of tweets to show on the page.
 *  @property {Date}            name                   - The date for which all the tweet must be greater than to be shown on the page.
 *  @property {Boolean}         order                 - The boolean determining if there is a start date or not.
 *
 */
function TweetColumn(screenName, name, order) {
    var self = this;

    self.screenName = "";
    self.name = "";
    self.order = -1;

    /**
     * @name init
     * @function
     * @memberOf appdirect.challenge.models.TweetColumn
     *
     * @description
     * This is the function used to init the object.
     */
    self.init = function(screenName, name, order) {
        self.setScreenName(screenName);
        self.setName(name);
        self.setOrder(order);
    };

    /**
     * @name setScreenName
     * @function
     * @memberOf appdirect.challenge.models.TweetColumn
     *
     * @description
     * This is a setter for the screenName property.
     */
    self.setScreenName = function(screenName) {
        if(!angular.isString(screenName)) {
            throw("The screenName you passed is not a string. You passed: '" + screenName + "'.")
        }

        self.screenName = screenName;
    };

    /**
     * @name setName
     * @function
     * @memberOf appdirect.challenge.models.TweetColumn
     *
     * @description
     * This is a setter for the name property.
     */
    self.setName = function(name) {
        if(!angular.isString(name)) {
            throw("The name you passed is not a string. You passed: '" + name + "'.")
        }

        self.name = name;
    };

    /**
     * @name setOrder
     * @function
     * @memberOf appdirect.challenge.models.TweetColumn
     *
     * @description
     * This is a setter for the order property.
     */
    self.setOrder = function(order) {
        if(!angular.isNumber(order)) {
            throw("The order you passed is not a Number. You passed: '" + order + "'.")
        }

        if(order < 0) {
            throw("The order you passed must be greater than or equal to zero. You passed: '" + order + "'.")
        }

        self.order = order;
    };

    /**
     * @name deserialize
     * @function
     * @memberOf appdirect.challenge.models.TweetColumn
     *
     * @description
     * This is the method used to deserialize a JSON string into the current object.
     */
    self.deserialize = function(serializedString) {
        var tweetColumn = JSON.parse(serializedString);

        // Reconvert it back to our object
        self.setScreenName(parameter.screenName);
        self.setName(parameter.name);
        self.setOrder(parameter.order);
    };

    /**
     * @name serialize
     * @function
     * @memberOf appdirect.challenge.models.TweetColumn
     *
     * @description
     * This is the method used to serialize the current object in JSON.
     */
    self.serialize = function() {
        return JSON.stringify(this);
    };

    // Call init method with these parameters when the object is instanciated.
    self.init(screenName, name, order);
}