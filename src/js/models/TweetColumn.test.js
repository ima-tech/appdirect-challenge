
describe( "TweetColumn model", function() {

    describe( "Create a TweetColumn object", function() {

        it( "should be created", function() {
            var tweetColumn = new TweetColumn("screenName", "Name", 30);

            expect(tweetColumn.order).toEqual(30);
            expect(tweetColumn.screenName).toEqual("screenName");
            expect(tweetColumn.name).toEqual("Name");
        } );
    } );
} );
