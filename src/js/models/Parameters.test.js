
describe( "Parameters model", function() {

    describe( "Create a Parameters object", function() {

        it( "should be created", function() {
            var parameters = new Parameters();
            var startDate = new Date();
            var endDate = new Date();

            parameters.setNumberOfTweets(30);
            parameters.setStartDate(startDate);
            parameters.setIsStartDate(true);
            parameters.setEndDate(endDate);
            parameters.setIsEndDate(false);
            parameters.setStyle("Light");

            expect(parameters.numberOfTweets).toEqual(30);
            expect(parameters.startDate).toEqual(startDate);
            expect(parameters.isStartDate).toBeTruthy();
            expect(parameters.endDate).toEqual(endDate);
            expect(parameters.isEndDate).toBeFalsy();
            expect(parameters.style).toEqual("Light");
        } );
    } );
} );
