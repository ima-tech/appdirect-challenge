if(angular === undefined) {
    throw ('You must add the angular-js 1+ library before adding this library');
}

// Init the Angular module
angular.module('appdirect.challenge', [
    'ui.router',
    'ui.bootstrap',
    'ui.bootstrap.datetimepicker',
    'angular-sortable-view'
]);