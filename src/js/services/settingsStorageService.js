/**
 * @class SettingsStorageService
 * @memberOf appdirect.challenge
 */
angular.module("appdirect.challenge").service("SettingsStorageService", ["$q", "$http", function($q, $http) {
    var self = this;
    self.localStorage = window.localStorage;

    /**
     * @name init
     * @function
     * @memberOf appdirect.challenge.SettingsStorageService
     *
     * @description
     * This is the function used to init the service.
     */
    self.init = function() {
    };

    /**
     * @name getTweetColumns
     * @function
     * @memberOf appdirect.challenge.SettingsStorageService
     *
     * @description
     * This is the function used to retrieve the tweet columns saved into the localStorage.
     */
    self.getTweetColumns = function () {
        // Retrieve the tweet columns from the local storage and parse them into JSON.
        var serializedTweetColumns = JSON.parse(self.localStorage.getItem("TweetColumns"));
        if(serializedTweetColumns == undefined) {
            return undefined;
        }

        // Now, we want to have our TweetColumn object to  use the setters  and getters so we rebuild the objects.
        var deserializedTweetColumns = [];

        serializedTweetColumns.forEach(function(serializedTweetColumn) {
            deserializedTweetColumns.push(new TweetColumn(serializedTweetColumn.screenName, serializedTweetColumn.name, serializedTweetColumn.order));
        });

        return deserializedTweetColumns;
    };

    /**
     * @name setTweetColumns
     * @function
     * @memberOf appdirect.challenge.SettingsStorageService
     *
     * @description
     * This is the function used to set the tweet columns in the local storage.
     */
    self.setTweetColumns = function(tweetColumns) {
        if(!angular.isArray(tweetColumns)) {
            throw("When calling the 'setTweetColumns' of the 'SettingsStorageService', you must pass an array. You passed a: '" + typeof(tweetColumns) + "'");
        }

        self.localStorage.setItem("TweetColumns", JSON.stringify(tweetColumns));
    };

    /**
     * @name getParameters
     * @function
     * @memberOf appdirect.challenge.SettingsStorageService
     *
     * @description
     * This is the function used to retrieve the parameters from the localStorage.
     */
    self.getParameters = function() {
        var parameters = self.localStorage.getItem("Parameters");
        if(parameters == undefined) {
            return undefined;
        }

        deserializedParameters = new Parameters();
        deserializedParameters.deserialize(parameters);
        
        return deserializedParameters;
    };

    /**
     * @name setParameters
     * @function
     * @memberOf appdirect.challenge.SettingsStorageService
     *
     * @description
     * This is the function used to set the parameters in the local storage.
     */
    self.setParameters = function(parameters) {
        if(parameters instanceof Parameters) {
            self.localStorage.setItem("Parameters", parameters.serialize());
        }
        else {
            throw("When calling the 'setParameters' of the 'SettingsStorageService', you must pass an object of type 'Parameter'.  You passed a: '" + typeof(parameters) + "'")
        }
    };

    self.init();

    return self;
}]);