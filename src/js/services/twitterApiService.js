/**
 * @class TwitterApiService
 * @memberOf appdirect.challenge
 */
angular.module("appdirect.challenge").service("TwitterApiService", ["$q", "$http", function($q, $http) {
    var self = this;
    var USER_TIMELINE_URL = "http://localhost:7890/1.1/statuses/user_timeline.json";

    /**
     * @name init
     * @function
     * @memberOf appdirect.challenge.TwitterApiService
     *
     * @description
     * This is the function used to init the service.
     */
    self.init = function() {
    };

    /**
     * @name getTweets
     * @function
     * @memberOf appdirect.challenge.TwitterApiService
     *
     *  @param {string}     screenName      - The Twitter screen name to retrieve the tweet from.
     *  @param {Number}     count           - The number of tweets to return.
     *  @param {Date}       startDate       - The date for which the tweets must be greater than in order to be returned.
     *  @param {Date}       endDate         - The date for which the tweets must be lesser than in order to be returned.
     *
     * @description
     * This is the function used to return all the tweets with the specified paramters.
     */
    self.getTweets = function(screenName, count, startDate, endDate) {
        var url_with_parameters = USER_TIMELINE_URL + "?count=" + count + "&screen_name=" + screenName;

        return $http.get(url_with_parameters, {timeout: 3000}).then(function(response) {
            // Rebuild the list of tweets that are contained between the startDate and endDate
            var tweets = [];
            response.data.forEach(function(tweet) {
                var tweetDate = new Date(tweet.created_at);

                if( (startDate == null || tweetDate > startDate) && (endDate == null || tweetDate < endDate) ) {
                    tweets.push(tweet);
                }
            });

            return tweets;
        });
    };

    self.init();

    return self;
}]);