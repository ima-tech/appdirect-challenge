/**
 * @class HomeController
 * @memberOf appdirect.challenge
 */
angular.module('appdirect.challenge').controller('HomeController', ['$scope', 'SettingsStorageService', 'TwitterApiService', function($scope, settingsStorageService, twitterApiService) {
    $scope.tweetColumns = [];
    $scope.tweets = {};
    $scope.parameters = {};
    $scope.tweetColumnsSortOptions = {
        axis: 'x'
    };

    /**
     * @name init
     * @function
     * @memberOf appdirect.challenge.HomeController
     *
     * @description
     * This is the function used to init the controller. This function retrieves and sets the parameters from the SettingsStorageService if there are not undefined. This method also calls
     * the twitterApiService to retrieve all the tweets based on the parameters defined in the SettingsStorageService.
     */
    $scope.init = function() {
        // Save here the default values if none are already defined
        if(settingsStorageService.getTweetColumns() == undefined) {
            var tweetColumns = [];

            tweetColumns.push(new TweetColumn("appdirect", "App Direct", 0));
            tweetColumns.push(new TweetColumn("laughingsquid", "Laughing Squid", 1));
            tweetColumns.push(new TweetColumn("techcrunch", "TechCrunch", 2));

            // Set up here the default values
            settingsStorageService.setTweetColumns(tweetColumns);
        }

        if(settingsStorageService.getParameters() == undefined) {
            var parameters = new Parameters();

            settingsStorageService.setParameters(parameters);
        }
        
        // Retrieve the parameters and the tweet columns from the storage.
        $scope.parameters = settingsStorageService.getParameters();
        $scope.tweetColumns = settingsStorageService.getTweetColumns();

        // Build the list of tweets by retrieving the tweets from the API.
        $scope.tweetColumns.forEach(function(tweetColumn) {
            twitterApiService.getTweets(tweetColumn.screenName, $scope.parameters.numberOfTweets, $scope.parameters.startDate, $scope.parameters.endDate).then(function(response) {
                $scope.tweets[tweetColumn.screenName] = response;
            }, function(error) {
                $scope.$emit('new-message', {message : "There was an error fetching the tweets from the server. The error is: '" + error.status + "'.", type:"error"});
            })
        })
    };

    /**
     * @name onColumnReordered
     * @function
     * @memberOf appdirect.challenge.HomeController
     *
     * @description
     * This is the function called when the columns are reordered.
     */
    $scope.onColumnReordered = function() {
        // Loop through all the tweet columns, the array will be sorted by the order they appear on screen. Now, we need to update our parameter named 'Order' since when the page is refreshed, we want the columns to
        // keep the order modified by the user.
        $scope.tweetColumns.forEach(function(tweetColumn, index) {
            // Set here the new column order
            tweetColumn.setOrder(index);
        });

        // Update the tweetColumns in the SettingsStorageService.
        settingsStorageService.setTweetColumns($scope.tweetColumns);

        $scope.$emit('new-message', {message : "The columns were successfully reordered and were saved to the local storage.", type:"success"});
    };

    $scope.init();
}]);