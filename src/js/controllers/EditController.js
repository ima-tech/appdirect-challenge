/**
 * @class EditController
 * @memberOf appdirect.challenge
 */

angular.module('appdirect.challenge').controller('EditController', ['$scope', 'SettingsStorageService',  function($scope, settingsStorageService) {
    $scope.parameters = new Parameters();

    /**
     * @name init
     * @function
     * @memberOf appdirect.challenge.EditController
     *
     * @description
     * This is the function used to init the controller. This function retrieves and sets the parameters from the SettingsStorageService if there are not undefined.
     */
    $scope.init = function() {
        if(settingsStorageService.getParameters() != undefined) {
            $scope.parameters = settingsStorageService.getParameters();
        }
    };

    /**
     * @name saveParameters
     * @function
     * @memberOf appdirect.challenge.EditController
     *
     * @description
     * This is the function used when the parameters must be saved into the SettingsStorageService
     */
    $scope.saveParameters = function() {
        settingsStorageService.setParameters($scope.parameters);

        // Emit the event upwards in the scope so the AppController can catch it and update the style right away.
        $scope.$emit('parameters-saved');

        $scope.$emit('new-message', {message : "The parameters were successfully saved", type:"success"});
    };

    /**
     * @name updateStartDate
     * @function
     * @memberOf appdirect.challenge.EditController
     *
     * @description
     * This is the function used when the isStartDate checkbox is modified. We want to remove the startDate if the isStartDate is unchecked.
     */
    $scope.updateStartDate = function() {
      if($scope.parameters.isStartDate == false) {
          $scope.parameters.startDate = null;
      }
    };

    /**
     * @name updateEndDate
     * @function
     * @memberOf appdirect.challenge.EditController
     *
     * @description
     * This is the function used when the isEndDate checkbox is modified. We want to remove the endDate if the isEndDate is unchecked.
     */
    $scope.updateEndDate = function() {
        if($scope.parameters.isEndDate == false) {
            $scope.parameters.endDate = null;
        }
    };

    // Methods used by the datepicker to verify that the start time is not after the end time and vice versa.
    $scope.startDateOnSetTime = function () {
        $scope.$broadcast('start-date-changed');
    };

    $scope.endDateOnSetTime = function () {
        $scope.$broadcast('end-date-changed');
    };

    $scope.startDateBeforeRender = function ($dates) {
        if ($scope.parameters.endDate) {
            var activeDate = moment($scope.parameters.endDate);

            $dates.filter(function (date) {
                return date.localDateValue() >= activeDate.valueOf()
            }).forEach(function (date) {
                date.selectable = false;
            })
        }
    };

    $scope.endDateBeforeRender = function ($view, $dates) {
        if ($scope.parameters.startDate) {
            var activeDate = moment($scope.parameters.startDate).subtract(1, $view).add(1, 'minute');

            $dates.filter(function (date) {
                return date.localDateValue() <= activeDate.valueOf()
            }).forEach(function (date) {
                date.selectable = false;
            })
        }
    };

    // Call the init method when this controller is instantiated.
    $scope.init();
}]);