describe('EditController', function() {
    beforeEach(module('appdirect.challenge'));

    var $controller;
    var $scope;

    beforeEach(inject(function(_$controller_,$rootScope){
        // The injector unwraps the underscores (_) from around the parameter names when matching
        $scope =  $rootScope.$new();
        $controller = _$controller_;
    }));

    describe('updateStartDate', function() {
        it("Sets the start date to null if isStartDate is false", function() {
            var controller = $controller('EditController', { $scope: $scope });
            $scope.parameters.isStartDate = false;

            // Call the method
            $scope.updateStartDate();
            expect($scope.parameters.startDate).toBeNull();
        });
    });
    describe('updateEndDate', function() {
        it("Sets the end date to null if isEndDate is false", function() {
            var controller = $controller('EditController', { $scope: $scope });
            $scope.parameters.isEndDate = false;

            // Call the method
            $scope.updateEndDate();
            expect($scope.parameters.endDate).toBeNull();
        });
    });
});