describe('AppController', function() {
    beforeEach(module('appdirect.challenge'));

    var $controller;
    var $scope;

    beforeEach(inject(function(_$controller_,$rootScope){
        // The injector unwraps the underscores (_) from around the parameter names when matching
        $scope =  $rootScope.$new();
        $controller = _$controller_;
    }));

    describe('GetLogoUrl', function() {
        it("returns the light logo url if the style is 'Light'", function() {
            var controller = $controller('AppController', { $scope: $scope });
            $scope.parameters.setStyle("Light");

            expect($scope.getLogoUrl()).toEqual('images/appdirect-logo-light-large.png');
        });

        it("returns the dark logo url if the style is 'Dark'", function() {
            var controller = $controller('AppController', { $scope: $scope });
            $scope.parameters.setStyle("Dark");

            expect($scope.getLogoUrl()).toEqual('images/appdirect-logo-1c-dark-large.png');
        });

        it("returns the dark logo url if the style is 'Blue'", function() {
            var controller = $controller('AppController', { $scope: $scope });
            $scope.parameters.setStyle("Blue");

            expect($scope.getLogoUrl()).toEqual('images/appdirect-logo-1c-dark-large.png');
        });
    });

    describe("GetBackgroundClass", function() {
       it("returns background-dark if the style is 'Dark'", function() {
           var controller = $controller('AppController', { $scope: $scope });
           $scope.parameters.setStyle("Dark");

           expect($scope.getBackgroundClass()).toEqual('background-dark');
       });

        it("returns background-dark if the style is 'Blue'", function() {
           var controller = $controller('AppController', { $scope: $scope });
           $scope.parameters.setStyle("Blue");

           expect($scope.getBackgroundClass()).toEqual('background-dark');
       });

        it("returns undefined if the style is 'Light'", function() {
           var controller = $controller('AppController', { $scope: $scope });
           $scope.parameters.setStyle("Light");

           expect($scope.getBackgroundClass()).toBeUndefined();
       });
    });
    describe("GetNavbarClass", function() {
       it("returns navbar-light bg-faded if the style is 'Light'", function() {
           var controller = $controller('AppController', { $scope: $scope });
           $scope.parameters.setStyle("Light");

           expect($scope.getNavbarClass()).toEqual('navbar-light bg-faded');
       });

        it("returns navbar-inverse bg-inverse if the style is 'Dark'", function() {
           var controller = $controller('AppController', { $scope: $scope });
           $scope.parameters.setStyle("Dark");

           expect($scope.getNavbarClass()).toEqual('navbar-inverse bg-inverse');
       });

        it("returns undefined if the style is 'blue'", function() {
           var controller = $controller('AppController', { $scope: $scope });
           $scope.parameters.setStyle("Blue");

            expect($scope.getNavbarClass()).toEqual('navbar-inverse bg-primary');
       });
    });
});