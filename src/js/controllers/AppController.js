/**
 * @class AppController
 * @memberOf appdirect.challenge
 */
angular.module('appdirect.challenge').controller('AppController', ['$scope', 'SettingsStorageService', '$timeout', function($scope, settingsStorageService, $timeout) {
    $scope.parameters = new Parameters();
    $scope.alert = {
        message: "",
        type: "error",
        display: false
    };

    /**
     * @name init
     * @function
     * @memberOf appdirect.challenge.AppController
     *
     * @description
     * This is the function used to init the controller. This function retrieves and sets the parameters from the SettingsStorageService if there are not undefined.
     */
    $scope.init = function() {
        if(settingsStorageService.getParameters() != undefined) {
            $scope.parameters = settingsStorageService.getParameters();
        }

        $scope.registerEventListeners();
    };

    /**
     * @name getAlertClass
     * @function
     * @memberOf appdirect.challenge.AppController
     *
     * @description
     * This is the function used to return the class of the alert message;
     */
    $scope.getAlertClass = function() {
        if($scope.alert.type == "error") {
            return "alert-danger";
        }
        else if($scope.alert.type == "success") {
            return "alert-success";
        }
    };

    /**
     * @name registerEventListeners
     * @function
     * @memberOf appdirect.challenge.AppController
     *
     * @description
     * This is the function used to register all the event listeners.
     */
    $scope.registerEventListeners = function() {
        // This event listener is called when the parameters are saved. This is used to update the style of the application without having to refresh the page.
        $scope.$on('parameters-saved', function (event, data) {
            if(settingsStorageService.getParameters() != undefined) {
                $scope.parameters = settingsStorageService.getParameters();
            }
        });

        // This event listener is called when a new message should be shown on the screen.
        $scope.$on('new-message', function (event, data) {
            $scope.alert.display = true;
            $scope.alert.message = data.message;
            $scope.alert.type = data.type;

            // After 5 seconds, hide the message.
            $timeout(function() {
                $scope.alert.display = false;
            }, 5000);
        });
    };

    /**
     * @name getLogoUrl
     * @function
     * @memberOf appdirect.challenge.AppController
     *
     * @description
     * This is the function used to return the URL of the AppDirect logo depending on the current chosen style.
     */
    $scope.getLogoUrl = function() {
        var style = $scope.parameters.style;

        if(style == "Light") {
            return "images/appdirect-logo-light-large.png";
        }
        else if(style == "Dark" || style == "Blue") {
            return "images/appdirect-logo-1c-dark-large.png";
        }
    };

    /**
     * @name getBackgroundClass
     * @function
     * @memberOf appdirect.challenge.AppController
     *
     * @description
     * This is the function used to return the background class depending on the current chosen style.
     */
    $scope.getBackgroundClass = function() {
        var style = $scope.parameters.style;

        if(style == "Dark" || style == "Blue") {
            return "background-dark";
        }
    };

    /**
     * @name getNavbarClass
     * @function
     * @memberOf appdirect.challenge.AppController
     *
     * @description
     * This is the function used to return the navbar class depending on the current chosen style.
     */
    $scope.getNavbarClass = function() {
        var style = $scope.parameters.style;
        
        if(style == "Light") {
            return "navbar-light bg-faded";
        }
        else if(style == "Dark") {
            return "navbar-inverse bg-inverse";
        }
        else if(style == "Blue") {
            return "navbar-inverse bg-primary";
        }
    };

    $scope.init();
}]);