# appdirect-code-challenge-solution

This project is coding challenge for the front end position open at AppDirect.

## Setup

Install the project dependencies:

`npm install`

`gulp`

## Running

Starts the static and proxy servers:

`npm start`

Access the app at:

`dist/index.html`

## Documentation

This project contains auto-generated documentation using jsdoc that can be built using

`gulp doc`

The doc is also build using the default `gulp` command.

This documentation is located under dist/docs

## Tests

This project contains tests that are run whenever you build this project using `gulp`. The tests are built
using Jasmine and ran using Karma.

